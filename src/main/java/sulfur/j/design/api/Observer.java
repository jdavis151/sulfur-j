package sulfur.j.design.api;

public interface Observer {
    void watch(Observable observable);

    void unwatch(Observable observable);

    void update();
}
