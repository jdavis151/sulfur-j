package sulfur.j.design.api;

public interface Observable {

    void pushUpdate();

    void removeReferences();
}
