package sulfur.j.design.api.impl;

import sulfur.j.design.api.Observable;
import sulfur.j.design.api.Observer;

import java.util.HashSet;
import java.util.Set;

public class DefaultObserver implements Observer {
    private Set<Observable> subjects = new HashSet<Observable>();

    @Override
    public void watch(Observable observable) {
        subjects.add(observable);
    }

    @Override
    public void unwatch(Observable observable) {
        subjects.remove(observable);
    }

    @Override
    public void update() {

    }
}
