package sulfur.j.design.api.impl;

import sulfur.j.design.api.Observable;

public class DefaultObservable implements Observable {

    /** {@inheritDoc} */
    @Override
    public void pushUpdate() {

    }

    /** {@inheritDoc} */
    @Override
    public void removeReferences() {

    }
}
